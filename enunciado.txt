1. Crea una carpeta para este ejercicio, en cualquier lugar fuera de esta misma carpeta. Copia los tres 
directorios a la nueva carpeta.
2. En la nueva carpeta, crea un repositorio Git.
3. Añade al índice los archivos de los directorios 1 y 2.
4. Comprueba con git status que el índice tiene lo que quieres.
5. Crea un commit con esos dos archivos.
6. Realiza una modificación en el texto del archivo 1/1.txt y otra en el texto del archivo 2/2.txt.
7. Añade al índice únicamente las modificaciones del archivo 2/2.txt.
8. Haz git status e interpreta la información que te da.
9. Crea un commit con las modificaciones que has hecho en el archivo 2/2.txt y con el archivo 3/3.txt.
10. Haz git status y comprueba que el stage está vacío y que en el working directory está el cambio del archivo 1/1.txt.
11. Desversiona el archivo 1/1.txt (es decir, dile a Git que no lo versione), pero sin eliminarlo.
12. Elimina el archivo 2/2.txt.
13. Crea un commit que almacene la eliminación de esos dos últimos archivos.
14. Haz git status y comprueba que todo está como esperas (el archivo 1/1.txt está sin versionar y el índice está sin
cambios).
15. Configura el repositorio para que ignore la carpeta 1.
16. Haz git status y comprueba la diferencia.
